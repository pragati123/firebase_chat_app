<!DOCTYPE html>
<html lang="en">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script src="https://www.gstatic.com/firebasejs/4.3.1/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyDLmujrPw4QJzaw1vlvjvT8EoMtgIv4myM",
            authDomain: "web-chat-a58e0.firebaseapp.com",
            databaseURL: "https://web-chat-a58e0.firebaseio.com",
            projectId: "web-chat-a58e0",
            storageBucket: "web-chat-a58e0.appspot.com",
            messagingSenderId: "1085549646569"
        };
        firebase.initializeApp(config);

        var isLoggedIn = false;
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                // User is signed in.
                isLoggedIn = true;
                // ...
            } else {
               isLoggedIn = false
            }
        });

        function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }
    </script>


    <style>
        .chat {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .chat li {
            margin-bottom: 10px;
            padding-bottom: 5px;
            border-bottom: 1px dotted #B3A9A9;
        }

        .chat li .chat-body p {
            margin: 0;
            color: #777777;
        }

        .panel-body {
            overflow-y: scroll;
            height: 350px;
        }

        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar-thumb {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }
    </style>

</head>


<body>
{{--<div id="app">--}}
{{--<div class="navbox">--}}
{{--<nav id="mainNav" class="navbar navbar-default navbar-fixed-top"--}}
{{--style="background: #fff;padding: 10px 10px; border-radius: 2px;box-shadow: 3px 4px 6px rgba(70,70,0,0.70);">--}}
{{--<div class="container">--}}
{{--<!-- Brand and toggle get grouped for better mobile display -->--}}
{{--<div class="navbar-header page-scroll">--}}
{{--<button type="button" class="navbar-toggle" data-toggle="collapse"--}}
{{--data-target="#bs-example-navbar-collapse-1">--}}
{{--<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>--}}
{{--</button>--}}
{{--<a class="navbar-brand"--}}
{{--style="color: #d43f3a;	font-family: 'Lobster', Georgia, Times, serif;font-size: 40px;line-height:22px;"--}}
{{--href="{{ url('/home') }}">ConnectInn</a>--}}
{{--</div>--}}

{{--<!-- Collect the nav links, forms, and other content for toggling -->--}}
{{--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">--}}
{{--<ul class="nav navbar-nav navbar-right">--}}

{{--@if (Auth::check())--}}
{{--<li>--}}
{{--<form class="navbar-form {{ $errors->has('filter') || $errors->has('query') ? ' has-error' : '' }}"--}}
{{--role="search" method="get" action={{url('/search')}}>--}}
{{--<div class="input-group add-on">--}}
{{--<input class="form-control" placeholder="Search" name="query" id="query"--}}
{{--type="text"--}}
{{--value="{{isset($query) == null? "" : $query}}">--}}
{{--<div class="input-group-btn">--}}
{{--<button class="btn btn-default" type="submit" style="background: lightgray">--}}
{{--<i--}}
{{--class="glyphicon glyphicon-search"></i></button>--}}
{{--</div>--}}
{{--</div>--}}

{{--</form>--}}
{{--</li>--}}
{{--<li><a href="{{url('/home')}}" style="color: firebrick">Home</a></li>--}}
{{--<li><a href="{{url('/myprofile')}}" style="color: firebrick">Profile</a></li>--}}
{{--<li><a href="{{url('/activities')}}" style="color: firebrick">Activities</a></li>--}}
{{--<li class="dropdown">--}}
{{--<a href="#" class="dropdown-toggle" style="color: firebrick" data-toggle="dropdown"--}}
{{--role="button" aria-expanded="false">--}}
{{--<img src="{{Auth::user()->profile_pic_url}}" height="25" width="25" alt="pic"--}}
{{--class="img-circle"> <span id="user_name">{{ Auth::user()->name }}</span> <span--}}
{{--class="caret"></span>--}}
{{--</a>--}}

{{--<ul class="dropdown-menu" role="menu">--}}
{{--<li><a style="color: #2F4F4F" href="{{url('/me/resume')}}" target="_blank" style="color: firebrick">Download--}}
{{--Resume</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a style="color: #2F4F4F" href="{{ url('/followers') }}">Followers</a>--}}
{{--</li>--}}

{{--<li role="separator" class="divider"></li>--}}

{{--<li>--}}
{{--<a style="color: #2F4F4F" href="{{ url('/logout') }}"--}}
{{--onclick="event.preventDefault();--}}

{{--document.getElementById('logout-form').submit();">--}}
{{--Logout <i class="fa fa-sign-out pull-right" aria-hidden="true"></i>--}}

{{--</a>--}}

{{--<form id="logout-form" action="{{ url('/logout') }}" method="POST"--}}
{{--style="display: none;">--}}
{{--{{ csrf_field() }}--}}
{{--</form>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</li>--}}

{{--@endif--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--<!-- /.navbar-collapse -->--}}

{{--<!-- /.container-fluid -->--}}
{{--</nav>--}}
{{--</div>--}}
{{--</div>--}}


<div>

    <div>

        @yield('content');


    </div>


</div>


</body>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

@yield('scripts')

</html>
