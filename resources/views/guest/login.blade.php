@extends('layouts.app')

@section('content')
    <br>
    <div class="col-md-8 col-md-push-2">
        <form id="submitLogin">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="signUpEmail" placeholder="Email">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="signUpPassword" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('#submitLogin').off('submit').submit(function (e) {
            e.preventDefault();
            console.log('called')

            var email = $('#signUpEmail').val();
            var password = $('#signUpPassword').val();
            firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
            });




        })
    </script>
@endsection