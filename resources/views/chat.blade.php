@extends('layouts.app')

@section('content')
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-push-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Chats</div>

                    <div class="panel-body" id="messageContainer">
                        <ul id="showMessage" class="chat"></ul>
                    </div>
                    <div class="panel-footer">
                        <div class="input-group">
                            <input id="message" type="text" name="message" class="form-control input-sm"
                                   autocomplete="off"
                                   placeholder="Type your message here...">
                            <span class="input-group-btn">
                                    <button class="btn btn-primary btn-sm" id="submitButton">
                                        Send</button>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        $('#submitButton').on('click', function (e) {
            e.preventDefault();
            var message = $('#message').val();
            firebase.database().ref().child('global-chat').push({
                'message': message,
                'name': firebase.auth().currentUser.email
            });
            $('#message').val(" ");
        });


        firebase.database().ref().child('global-chat').on('child_added', function (snapshot) {
            console.log('message added');
            var chat = snapshot.val();
            addMessage(chat);
            scrollContainer();
        });

        function addMessage(chat) {
            $('#showMessage').append(getLiElement(chat.name, chat.message));
        }

        var scrollContainer = function () {
            const messageContainer = $("#messageContainer");
            messageContainer.animate({ scrollTop: messageContainer.prop("scrollHeight")}, 5);
            console.log('scrolled');
        };

        var getLiElement = function (email, message) {
            return "<li class='left clearfix'> <div class='chat-body clearfix'> <div class='header'> <strong class='primaryfont'>" + email + "</strong> </div> <p> " + message + "</p> </div> </li> "
        }
    </script>

@endsection