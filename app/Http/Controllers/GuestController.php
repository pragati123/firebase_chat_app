<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function signUp() {
        return view('guest.sign-up');
    }

    public function login(){
        return view('guest.login');
    }

    public function sendMessage(){
        return view('chat');
    }
}
